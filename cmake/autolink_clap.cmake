# Copyright (C) 2024 The Qt Company Ltd.
# SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-Qt-Commercial

if(UNIX)
    if(APPLE)
        set(CLAP_SYSTEM_PATH "/Library/Audio/Plug-Ins/CLAP")
        set(CLAP_USER_PATH "$ENV{HOME}/Library/Audio/Plug-Ins/CLAP")
    else()
        set(CLAP_SYSTEM_PATH "/usr/lib/clap")
        set(CLAP_USER_PATH "$ENV{HOME}/.clap")
    endif()
elseif(WIN32)
    set(CLAP_SYSTEM_PATH "$ENV{COMMONPROGRAMFILES}\\CLAP")
    set(CLAP_USER_PATH "$ENV{LOCALAPPDATA}\\Programs\\Common\\CLAP")
else()
    message(FATAL_ERROR "Unsupported platform")
endif()

function(create_symlink_target_clap TARGET_NAME)
    set_target_properties(${TARGET_NAME} PROPERTIES SUFFIX ".clap" PREFIX "")
    internal_create_symlink_target(${TARGET_NAME} ${CLAP_USER_PATH}/${PROJECT_NAME})
endfunction()

function(create_symlink_target_gui TARGET_NAME)
    internal_create_symlink_target(${TARGET_NAME} ${CLAP_USER_PATH}/${PROJECT_NAME})
endfunction()

function(internal_create_symlink_target TARGET_NAME PLUGIN_OUTPUT_DIRECTORY)
    # Ensure the output directory exists
    if(NOT EXISTS ${PLUGIN_OUTPUT_DIRECTORY})
        message(STATUS "Creating output directory: ${PLUGIN_OUTPUT_DIRECTORY}")
        file(MAKE_DIRECTORY "${PLUGIN_OUTPUT_DIRECTORY}")
    endif()

    # Create a custom command to create the symlink
    add_custom_command(
            TARGET ${TARGET_NAME} POST_BUILD
            COMMAND ${CMAKE_COMMAND} -E create_symlink
            "$<TARGET_FILE:${TARGET_NAME}>"
            "${PLUGIN_OUTPUT_DIRECTORY}/$<TARGET_FILE_NAME:${TARGET_NAME}>"
    )

    add_custom_target(
            create_symlink_${TARGET_NAME} ALL
            DEPENDS ${TARGET_NAME}
            COMMENT "Creating symlink for ${TARGET_NAME}"
    )

    add_dependencies(create_symlink_${TARGET_NAME} ${TARGET_NAME})

    add_custom_command(
            TARGET create_symlink_${TARGET_NAME} POST_BUILD
            COMMAND ${CMAKE_COMMAND} -E echo "Symlink created: ${PLUGIN_OUTPUT_DIRECTORY}/$<TARGET_FILE_NAME:${TARGET_NAME}>"
    )
endfunction()
