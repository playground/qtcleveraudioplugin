// Copyright (C) 2024 The Qt Company Ltd.
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-Qt-Commercial

#ifndef QCLAPINTERFACE_H
#define QCLAPINTERFACE_H

#include "api.qpb.h"
#include "api_client.grpc.qpb.h"
using namespace api::v0;

#include <QtCore/qobject.h>
#include <QtCore/qtimer.h>
#include <QtGui/qwindow.h>
#include <QtQml/qqml.h>

#include <map>

class QClapInterface : public QObject
{
    Q_OBJECT
    QML_SINGLETON
    QML_NAMED_ELEMENT(ClapInterface)
    QML_UNCREATABLE("QClapInterface is a singleton")

    Q_PROPERTY(PluginState state READ state NOTIFY stateChanged FINAL)
    Q_PROPERTY(bool visible READ visible WRITE setVisible NOTIFY visibleChanged)
    Q_PROPERTY(QWindow* transientParent READ transientParent NOTIFY transientParentChanged FINAL)

    using Param = std::pair<ClapEventParam, ClapEventParamInfo>;
    using Id = quint32;

public:
    explicit QClapInterface(QObject* parent = nullptr);
    void connect(const QString &address, const QString &hash);
    // static int typeId()  noexcept { return m_typeId; } // TODO: use typeId

    enum PluginState {
        Created = 0, Connected, Active, Inactive, Disconnected, Error
    }; Q_ENUM(PluginState)
    enum NoteMapping {
        C3 = 60, Csh3, D3, Dsh3, E3, F3, Fsh3, G3, Gsh3, A3, Ash3, B3,
        C4, Csh4, D4, Dsh4, E4, F4, Fsh4, G4, Gsh4, A4, Ash4, B4
    }; Q_ENUM(NoteMapping)

    PluginState state() const;
    bool visible() const;
    void setVisible(bool visible);
    QWindow* transientParent() const;
    // TODO: Create a Param and Note C++/QML Type, this is just an easy fix for now.
    Q_INVOKABLE ClapEventParam param(Id paramId) const;
    Q_INVOKABLE ClapEventParamInfo paramInfo(Id paramId) const;
    Q_INVOKABLE void enqueueParam(Id paramId, double value);

    void addParam(const ClapEventParam &param);
    void addParamInfo(const ClapEventParamInfo &info);

signals:
    void stateChanged();
    void visibleChanged();
    void transientParentChanged();

    // TODO: signal with types?
    void paramChanged();
    void paramInfoChanged();

    void noteReceived(const ClapEventNote &note);

private:
    void processEvents(const ServerEvents &events);
    ClientEvent create(EventGadget::Event evTag);
    void pollingCallback();

private:
    QUrl url = {};
    QGrpcMetadata metadata = {};
    std::unique_ptr<ClapInterface::Client> client = {};
    std::shared_ptr<QGrpcServerStream> stream;
    ClientParams mParamsToSend = {};
    QTimer callbackTimer;

    bool mVisible = false;
    QWindow *mHostWindow = nullptr;
    PluginState mState = Created;
    // TODO: The Id has the raw value from module.h not taking the ParamOffset into account. Potential problem?
    std::map<Id, Param> mParams = {};
    Q_DISABLE_COPY_MOVE(QClapInterface)
};

#endif // CLAPINTERFACE_H
