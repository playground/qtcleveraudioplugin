// Copyright (C) 2024 The Qt Company Ltd.
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-Qt-Commercial

#ifndef QNOTE_H
#define QNOTE_H

#include <api.qpb.h>
using namespace api::v0;

#include <QtCore/qobject.h>
#include <QtQml/qqml.h>

class QNote : public QObject
{
    Q_OBJECT
    Q_PROPERTY(NoteMapping key READ key WRITE setKey NOTIFY keyChanged FINAL)
    Q_PROPERTY(ClapEventNote* data READ data NOTIFY noteChanged FINAL)
    Q_PROPERTY(bool played READ played NOTIFY playedChanged FINAL)
    QML_NAMED_ELEMENT(Note)

public:
    QNote(QObject *parent = nullptr) : QObject(parent) {}
    enum NoteMapping {
        C0 = 24, Csh0, D0, Dsh0, E0, F0, Fsh0, G0, Gsh0, A0, Ash0, B0,
        C1 = 36, Csh1, D1, Dsh1, E1, F1, Fsh1, G1, Gsh1, A1, Ash1, B1,
        C2 = 48, Csh2, D2, Dsh2, E2, F2, Fsh2, G2, Gsh2, A2, Ash2, B2,
        C3 = 60, Csh3, D3, Dsh3, E3, F3, Fsh3, G3, Gsh3, A3, Ash3, B3,
        C4 = 72, Csh4, D4, Dsh4, E4, F4, Fsh4, G4, Gsh4, A4, Ash4, B4,
        C5 = 84, Csh5, D5, Dsh5, E5, F5, Fsh5, G5, Gsh5, A5, Ash5, B5,
        C6 = 96, Csh6, D6, Dsh6, E6, F6, Fsh6, G6, Gsh6, A6, Ash6, B6,
       C7 = 108, Csh7, D7, Dsh7, E7, F7, Fsh7, G7, Gsh7, A7, Ash7, B7,
    }; Q_ENUM(NoteMapping)

    ClapEventNote* data() { return &mNote; }
    void setData(const ClapEventNote& note)
    {
        if (note == mNote)
            return;
        mNote = note;
        // TODO: use (lambda) binding?
        if (mNote.type() == ClapEventNote::Type::NoteOn)
            setPlayed(true);
        else if (mNote.type() == ClapEventNote::Type::NoteOff)
            setPlayed(false);
        emit noteChanged();
    }

    NoteMapping key() const { return static_cast<NoteMapping>(mNote.key()._t); }
    void setKey(NoteMapping key)
    {
        if (key == mNote.key())
            return;
        mNote.setKey(key);
        emit keyChanged();
    }

    bool played() const { return mPlayed; }
    void setPlayed(bool played)
    {
        if (mPlayed == played)
            return;
        mPlayed = played;
        emit playedChanged();
    }

signals:
    void noteChanged();
    void keyChanged();
    void playedChanged();

private:
    ClapEventNote mNote = {};
    bool mPlayed = false;
};



#endif // QNOTE_H
