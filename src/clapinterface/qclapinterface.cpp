// Copyright (C) 2024 The Qt Company Ltd.
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-Qt-Commercial

#include "qclapinterface.h"

#include <QtCore/qurl.h>
#include <QtCore/qthread.h>
#include <QtCore/qflags.h>
#include <QtCore/qdatetime.h>
#include <QtGui/qguiapplication.h>
#include <QtQuick/qquickview.h>
#include <QtGrpc/QtGrpc>

namespace Metadata {
    static constexpr std::string_view PluginHashId = "plugin-hash-id";
}

QClapInterface::QClapInterface(QObject *parent)
    : QObject(parent), client(std::make_unique<ClapInterface::Client>(this))
{
    qRegisterProtobufTypes();
    callbackTimer.setInterval(7);
    callbackTimer.callOnTimeout(this, &QClapInterface::pollingCallback);
}

void QClapInterface::connect(const QString &address, const QString &hash)
{
    // TODO: add configururation for unix domain sockets
    // get the last part of the address, the port e.g: 0.0.0.0:55176. only get 55176
    const auto port = address.split(":").last().toInt();

    url = QUrl(QString("http://localhost:") + QString::number(port));
    qDebug() << "Connecting to url: " << url << " hash: " << hash;
    // Extract the port

    QGrpcChannelOptions channelOptions(url);
    metadata = {
        { QByteArray(Metadata::PluginHashId.data()), { hash.toUtf8() } },
    };
    channelOptions.withMetadata(metadata);

    auto channel = std::make_shared<QGrpcHttp2Channel>(channelOptions);
    client->attachChannel(channel);
    // Start the server side stream
    stream = client->streamServerEventStream(ClientRequest(), {});

    QObject::connect(stream.get(), &QGrpcServerStream::errorOccurred, this,
        [](const QGrpcStatus &status) {
            qDebug() << "Client received error: " << status.code() << " msg: " << status.message();
            QGuiApplication::quit();
        }
    );

    QObject::connect(stream.get(), &QGrpcServerStream::finished, this,
         []() {
            qDebug() << "Client received finished signal";
            QGuiApplication::quit();
         }
    );

    QObject::connect(stream.get(), &QGrpcServerStream::messageReceived, this,
        [this]() { processEvents(stream->read<ServerEvents>()); }
    );

    callbackTimer.start();
}

QClapInterface::PluginState QClapInterface::state() const
{
    return mState;
}

bool QClapInterface::visible() const
{
    return mVisible;
}

void QClapInterface::setVisible(bool visible)
{
    if (mVisible == visible)
        return;
    mVisible = visible;
    emit visibleChanged();
}

void QClapInterface::addParam(const ClapEventParam &param)
{
    auto it = mParams.find(param.paramId());
    if (it == mParams.end()) {
        qDebug() << "Param is not contained: " << param.paramId();
        return;
    }
    if (it->second.first == param)
        return;
    it->second.first = param;
    emit paramChanged();
}

void QClapInterface::enqueueParam(Id paramId, double value)
{
    auto it = mParams.find(paramId);
    if (it == mParams.end()) {
        qDebug() << "Param is not contained: " << paramId;
        return;
    }
    it->second.first.setValue(value);

    ClapEventParam p;
    p.setParamId(paramId);
    p.setValue(value);
    p.setType(ClapEventParam::Type::Value);

    TimestampMsg ts;
    ts.setSeconds(QDateTime::currentSecsSinceEpoch());
    ts.setNanos(QDateTime::currentMSecsSinceEpoch() * 1000);

    ClientParam clientParam;
    clientParam.setEvent(EventGadget::Param);
    clientParam.setTimestamp(std::move(ts));
    clientParam.setParam(std::move(p));
    mParamsToSend.params().push_back(std::move(clientParam));
}

void QClapInterface::addParamInfo(const ClapEventParamInfo &info)
{
    auto data = std::make_pair(ClapEventParam(), info);
    auto it = mParams.find(info.paramId());
    if (it == mParams.end()) {
        mParams.insert(std::make_pair(info.paramId(), std::move(data)));
        return emit paramInfoChanged();
    }
    if (it->second.second == info)
        return;
    it->second.second = info;
    emit paramInfoChanged();
}

ClapEventParam QClapInterface::param(QClapInterface::Id paramId) const
{
    auto it = mParams.find(paramId);
    if (it == mParams.end()) {
        qDebug() << "Param is not contained: " << paramId;
        return ClapEventParam(); // TODO: error handling
    }
    return it->second.first;
}

ClapEventParamInfo QClapInterface::paramInfo(QClapInterface::Id paramId) const
{
    auto it = mParams.find(paramId);
    if (it == mParams.end()) {
        qDebug() << "ParamInfo is not contained: " << paramId;
        return ClapEventParamInfo(); // TODO: error handling
    }
    return it->second.second;
}

QWindow *QClapInterface::transientParent() const
{
    return mHostWindow;
}

void QClapInterface::processEvents(const ServerEvents &events)
{
    for (const auto &event : events.events()) {
        switch (event.event()) {

            case EventGadget::PluginActivate: {
                mState = Active;
                emit stateChanged();
            } break;

            case EventGadget::PluginDeactivate: {
                mState = Inactive;
                emit stateChanged();
            } break;

            case EventGadget::GuiCreate: {
                auto call = client->ClientEventCall(create(EventGadget::GuiCreate));
                qDebug() << "GuiCreate successfully registered with server";
                mState = Connected;
                emit stateChanged();
            } break;

            case EventGadget::GuiShow: {
                client->ClientEventCall(create(EventGadget::GuiShow));
                setVisible(true);
            } break;

            case EventGadget::GuiHide: {
                const auto res = client->ClientEventCall(create(EventGadget::GuiHide));
                setVisible(false);
            } break;

            case EventGadget::GuiSetTransient: {
                if (!event.hasMainSync()) {
                    qDebug() << "GuiSetTransient: no serverValue";
                }
                qDebug() << "GuiSetTransient: " << event.mainSync().windowId();
                const auto res = client->ClientEventCall(create(EventGadget::GuiSetTransient));
                WId wid = static_cast<WId>(event.mainSync().windowId());

                QWindow *w = QWindow::fromWinId(wid);
                if (w && w != mHostWindow) { // Only change if we need to
                    qDebug() << "Setting transient parent success";
                    mHostWindow = w;
                    emit transientParentChanged();
                    return;
                } // TODO: else signal misbehavior
                qDebug() << "Setting transient parent failed";
            } break;

            case EventGadget::GuiDestroy: {
                qDebug() << "GuiDestroy";
                const auto res = client->ClientEventCall(create(EventGadget::GuiDestroy));
            } break;

            case EventGadget::Param: {
                if (!event.hasParam())
                    return;
                addParam(event.param());
            } break;

            case EventGadget::ParamInfo: {
                if (!event.hasParamInfo())
                    return;
                addParamInfo(event.paramInfo());
            } break;

            case EventGadget::Note: {
                if (!event.hasNote())
                    return;
                emit noteReceived(event.note());
            } break;

            case EventGadget::EventSuccess:
                qDebug() << "EventSuccess: ";
                break;
            case EventGadget::EventFailed:
                qDebug() << "EventFailed: ";
                break;
            case EventGadget::EventInvalid:
                qDebug() << "EventInvalid: ";
                break;
            default:
                qDebug() << "Unknown event: " << event.event();
                break;
        }
    }
}

ClientEvent QClapInterface::create(EventGadget::Event evTag)
{
    ClientEvent ev;
    ev.setEvent(evTag);
    return ev;
}

void QClapInterface::pollingCallback()
{
    if (mParamsToSend.params().empty())
        return;
    client->ClientParamCall(mParamsToSend);
    mParamsToSend.params().clear();
}
