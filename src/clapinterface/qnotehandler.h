// Copyright (C) 2024 The Qt Company Ltd.
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-Qt-Commercial

#ifndef QTCLAPINTERFACE_QNOTE_HPP
#define QTCLAPINTERFACE_QNOTE_HPP

#include <api.qpb.h>
using namespace api::v0;
#include "qnote.h"
#include "qclapinterface.h"

#include <QtCore/qobject.h>
#include <QtQml/qqml.h>
#include <QtQml/qqmllist.h>
#include <QtQml/qqmlengine.h>
#include <QtQml/qqmlcontext.h>

class QNoteHandler : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QQmlListProperty<QNote> notes READ notes)
    Q_CLASSINFO("DefaultProperty", "notes")
    QML_NAMED_ELEMENT(NoteHandler)
public:
    QNoteHandler(QObject *parent = nullptr) : QObject(parent)
    {
    }
    void setNoteData(const ClapEventNote& note)
    {
        bool globalKey = note.key() == -1;
        bool globalChannel = note.channel() == -1;
        bool offOrChoke = (note.type() == ClapEventNote::Type ::NoteOff || note.type() == ClapEventNote::Type ::NoteChoke);

        for (auto *n : mNotes) {
            if (offOrChoke && globalKey && globalChannel)
                n->setData(note);
            else if (offOrChoke && globalKey && n->data()->channel() == note.channel())
                n->setData(note);
            else if (offOrChoke && globalChannel && n->data()->key() == note.key())
                n->setData(note);
            else if (n->data()->key() == note.key() && n->data()->channel() == note.channel())
                n->setData(note);
        }
    }

    QQmlListProperty<QNote> notes()
    {
        return {
            this, this,
            &QNoteHandler::appendFn,
            &QNoteHandler::countFn,
            &QNoteHandler::atFn,
            &QNoteHandler::clearFn,
            &QNoteHandler::replaceFn,
            &QNoteHandler::removeLastFn
        };
    }
    void appendNote(QNote *note)
    {
        // This seems wrong... How can I access the engine and the singleton more directly?
        connectHandler();
        mNotes.append(note);
    }
    qsizetype noteCount() const
    {
        return mNotes.count();
    }
    QNote *noteAt(qsizetype index) const
    {
        return mNotes.at(index);
    }
    void clearNotes()
    {
        mNotes.clear();
    }
    void replaceNote(qsizetype index, QNote *note)
    {
        mNotes.replace(index, note);
    }
    void removeLastNote()
    {
        mNotes.removeLast();
    }
private:
    void connectHandler()
    {
        if (mInterface == nullptr) {
            QQmlEngine *engine = QQmlEngine::contextForObject(this)->engine();
            mInterface = engine->singletonInstance<QClapInterface*>("Clap.Interface","ClapInterface");
            assert(mInterface != nullptr);
            QObject::connect(mInterface, &QClapInterface::noteReceived, this, &QNoteHandler::setNoteData);
        }
    }
    static void appendFn(QQmlListProperty<QNote> *list, QNote *note)
    {
        reinterpret_cast<QNoteHandler*>(list->data)->appendNote(note);
    }
    static qsizetype countFn(QQmlListProperty<QNote> *list)
    {
        return reinterpret_cast<QNoteHandler*>(list->data)->noteCount();
    }
    static QNote* atFn(QQmlListProperty<QNote> *list, qsizetype index)
    {
        return reinterpret_cast<QNoteHandler*>(list->data)->noteAt(index);
    }
    static void clearFn(QQmlListProperty<QNote> *list)
    {
        reinterpret_cast<QNoteHandler*>(list->data)->clearNotes();
    }
    static void replaceFn(QQmlListProperty<QNote> *list, qsizetype index, QNote *note)
    {
        reinterpret_cast<QNoteHandler*>(list->data)->replaceNote(index, note);
    }
    static void removeLastFn(QQmlListProperty<QNote> *list)
    {
        reinterpret_cast<QNoteHandler*>(list->data)->removeLastNote();
    }

private:
    QList<QNote*> mNotes = {};
    QClapInterface *mInterface = nullptr;
};


#endif //QTCLAPINTERFACE_QNOTE_HPP
