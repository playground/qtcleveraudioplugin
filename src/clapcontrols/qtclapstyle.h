// Copyright (C) 2024 The Qt Company Ltd.
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-Qt-Commercial

#ifndef QTCLAPCONTROLS_H
#define QTCLAPCONTROLS_H

#include <QtCore/qobject.h>
#include <QtCore/qmap.h>
#include <QtCore/qstring.h>
#include <QtCore/qanystringview.h>
#include <QtCore/qdiriterator.h>
#include <QtGui/qcolor.h>
#include <QtQml/qqml.h>

class QClapStyle : public QObject
{
    Q_OBJECT
    QML_ELEMENT
    QML_SINGLETON
    QML_UNCREATABLE("QtClapStyle is an utility class")

    Q_PROPERTY(QStringList themes READ themes NOTIFY themeChanged FINAL)
    Q_PROPERTY(QStringList fonts READ fonts NOTIFY fontChanged FINAL)

    struct NamePath { QString name, path; };
    static constexpr QAnyStringView QrcBasePath = ":/qt/qml/Clap/Controls/";
public:
    explicit QClapStyle(QObject* parent = nullptr);

    bool initThemes(QAnyStringView schemeDir = QString(QrcBasePath.toString() + "themes"));
    bool initFonts(QAnyStringView fontDir = QString(QrcBasePath.toString() + "fonts"));

    Q_INVOKABLE bool loadTheme(qsizetype idx);
    Q_INVOKABLE bool loadFont(qsizetype idx);
    Q_INVOKABLE QColor color(QString name) const;

    QString activeTheme(qsizetype idx) const;
    QString activeFont(qsizetype idx) const;

    QStringList themes() const;
    QStringList fonts() const;

signals:
    void themeChanged();
    void fontChanged();

private:
    std::map<QString, QColor> activeColors;
    QList<NamePath> themesList;
    QList<NamePath> fontsList;
    qsizetype activeThemeIdx = -1;
    qsizetype activeFontIdx = -1;
    int activeFontId = -1;
};


#endif // QTCLAPCONTROLS_H
