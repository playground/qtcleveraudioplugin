// Copyright (C) 2024 The Qt Company Ltd.
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-Qt-Commercial

import QtQuick
import QtQuick.Controls.Basic

import Clap.Interface
import Clap.Controls

ApplicationWindow {
    id: ctrl

    property bool pluginActive: true
    readonly property color cBg: QClapStyle.color("bg")

    visible: true
    flags: Qt.Window | Qt.WindowStaysOnTopHint | Qt.CustomizeWindowHint
    color: pluginActive ? cBg : Qt.tint(cBg, QClapStyle.color("tint"))

    Connections {
        target: ClapInterface
        function onVisibleChanged() {
            ctrl.visible = ClapInterface.visible;
        }
        function onTransientParentChanged() {
            console.log("transient parent changed " + ClapInterface.transientParent);
            ctrl.transientParent = ClapInterface.transientParent;
        }
        // function onStateChanged() {
        //     ctrl.pluginActive = QClapInterface.state == QClapInterface.Active;
        // }
    }

    Component.onCompleted: {
        QClapStyle.loadFont(0);
    }
}
