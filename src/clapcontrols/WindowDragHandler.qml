// Copyright (C) 2024 The Qt Company Ltd.
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-Qt-Commercial

import QtQuick
import QtQuick.Controls

// Allows dragging the window when placed on an unused section of the UI.
DragHandler {

    required property var dragWindow

    target: null
    onActiveChanged: {
        if (active) dragWindow.startSystemMove()
    }
}
