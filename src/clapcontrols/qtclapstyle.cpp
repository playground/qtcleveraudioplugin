// Copyright (C) 2024 The Qt Company Ltd.
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-Qt-Commercial

#include "qtclapstyle.h"

#include <toml.hpp>

#include <QtCore/qfile.h>
#include <QtGui/qguiapplication.h>
#include <QtCore/qdir.h>

#include <QtGui/qfont.h>
#include <QtGui/qfontdatabase.h>

std::istringstream readQrcToStdStream(const QString& path)
{
    // This is sadly needed to convert between qrc <> std::istream
    QFile f(path);
    if (!f.open(QIODevice::ReadOnly | QIODevice::Text))
        std::terminate();
    QTextStream qts(&f);
    std::istringstream iss(qts.readAll().toStdString());
    return iss;
}

void printResourceDir()
{
    QDirIterator it(":", QDirIterator::Subdirectories);
    while (it.hasNext()) {
        qDebug() << it.next();
    }
}

QClapStyle::QClapStyle(QObject *parent) : QObject(parent)
{
    if (initThemes()) {
        if (!loadTheme(0))
            qDebug() << "Failed to load default theme";
    } else {
        qDebug() << "Failed to initialize schemes";
    }

    if (initFonts()) {
        if (!loadFont(0))
            qDebug() << "Failed to load default font";
    } else {
        qDebug() << "Failed to initialize fonts";
    }
}

bool QClapStyle::initThemes(QAnyStringView schemeDir)
{
    QDirIterator it(schemeDir.toString(), { "*.toml" }, QDir::Files, QDirIterator::Subdirectories);

    while (it.hasNext()) {
        auto fi = it.nextFileInfo();

        auto ss = readQrcToStdStream(fi.absoluteFilePath());
        const auto tomlData = toml::parse(ss);
        const auto rootTable = toml::get<toml::table>(tomlData); // All tables in files

        for (const auto& subTable : rootTable) {
            auto temp = toml::get<toml::table>(subTable.second); // Get individual entries
            themesList.push_back({QString::fromStdString(subTable.first), fi.filePath() });
        }
    }

    return !themesList.empty();
}

bool QClapStyle::loadTheme(qsizetype idx)
{
    // Search for the specified theme name
    if (idx < 0 || idx >= themesList.size()) {
        qDebug() << "Theme index out of bounds";
        return false;
    }

    // As the toml file can contain multiple schemes, make sure to only load one table
    const auto &e = themesList.at(idx);
    auto ss = readQrcToStdStream(e.path);
    const auto tomlData = toml::parse(ss);
    const auto keyVal = toml::find<toml::table>(tomlData, e.name.toStdString());

    for (const auto &i : keyVal) {
        activeColors.insert({
            QString::fromStdString(i.first),
            QColor(QString::fromStdString(i.second.as_string().str))
        });
    }
    emit themeChanged();

    return true;
}

bool QClapStyle::initFonts(QAnyStringView fontDir)
{
    QDirIterator it(fontDir.toString(), { "*.ttf", "*.otf" }, QDir::Files, QDirIterator::Subdirectories);
    while (it.hasNext()) {
        auto fi = it.nextFileInfo();
        fontsList.push_back({ fi.baseName(), fi.filePath() });
    }
    return !fontsList.empty();
}

QColor QClapStyle::color(QString name) const
{
    if (auto it = activeColors.find(name); it != activeColors.end())
        return it->second;
    return {};
}

bool QClapStyle::loadFont(qsizetype idx)
{
    if (idx < 0 || idx >= fontsList.size()) {
        qDebug() << "Font index out of bounds";
        return false;
    }

    const auto &e = fontsList.at(idx);

    if (activeFontId != -1) {
        if (!QFontDatabase::removeApplicationFont(activeFontId)) {
            qFatal() << "Failed to remove font: " << activeFontId;
            return false;
        }
    }

    activeFontId = QFontDatabase::addApplicationFont(e.path);
    if (activeFontId == -1) {
        qFatal() << "Failed to add font: " << e.path;
        return false;
    }

    auto families = QFontDatabase::applicationFontFamilies(activeFontId);
    if (families.empty()) {
        qFatal() << "Failed to get font family";
        activeFontId = -1;
        return false;
    }

    QGuiApplication::setFont(QFont(families[0]));
    emit fontChanged();

    return true;
}

QString QClapStyle::activeTheme(qsizetype idx) const
{
    if (idx < 0 || idx >= themesList.size())
        return {"Theme index out of bounds"};
    return themesList.at(idx).name;
}

QString QClapStyle::activeFont(qsizetype idx) const
{
    if (idx < 0 || idx >= fontsList.size())
        return {"Font index out of bounds"};
    return fontsList.at(idx).name;
}

QStringList QClapStyle::themes() const
{
    // TODO: cache
    QStringList ths;
    ths.reserve(themesList.size());
    for (const auto& i : themesList)
        ths.push_back(i.name);

    return ths;
}

QStringList QClapStyle::fonts() const
{
    // TODO: cache
    QStringList fnts;
    fnts.reserve(fontsList.size());
    for (const auto& i : fontsList)
        fnts.push_back(i.name);

    return fnts;
}

