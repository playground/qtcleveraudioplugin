// Copyright (C) 2024 The Qt Company Ltd.
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-Qt-Commercial

import QtQuick
import QtQuick.Controls.Basic

import Clap.Controls

Menu {
    id: root

    property color cBg: QClapStyle.color("bg1")
    property color cText: QClapStyle.color("fg")
    property color cMenuHighlight: QClapStyle.color("blue")

    delegate: MenuItem {
        id: menuItem
        contentItem: Item {
            Text {
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.leftMargin: 5

                text: menuItem.text
                color: root.cText
            }
            Rectangle {
                id: indicator

                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right
                width: 6
                height: parent.height

                visible: menuItem.highlighted
                color: root.cMenuHighlight
            }
        }
        background: Rectangle {
            implicitWidth: 210
            implicitHeight: 35
            color: "transparent"
        }
    }
    background: Rectangle {
        implicitWidth: 210
        implicitHeight: 35
        color: root.cBg
    }
}
