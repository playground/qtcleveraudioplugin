// Copyright (C) 2024 The Qt Company Ltd.
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-Qt-Commercial

import QtQuick
import QtQuick.Layouts
import QtQuick.Controls

import Clap.Interface
import Clap.Controls

Item {
    id: ctrl
    implicitWidth: 400
    implicitHeight: 100
    readonly property int nKeys: 64
    readonly property int noteWidth: 50
    readonly property int noteHeight: 200
    readonly property int sharpWidth: noteWidth * 0.75
    readonly property int sharpHeight: noteHeight * 0.75
    // Idea: simple c++ qml type that maps to all color options: active, disabled, hovered, normal
    property color cNote: QClapStyle.color("fg1")
    property color cNoteHov: QClapStyle.color("fg3")
    property color cNoteBorder: QClapStyle.color("bg0_h")
    property color cText: QClapStyle.color("bg0_h")

    property color cSharp: QClapStyle.color("bg0_h")
    property color cSharpHov: QClapStyle.color("bg2")
    property color cTextSharp: QClapStyle.color("fg1")

    NoteHandler {
        Note { id: c3; key: Note.C3 }
        Note { id: csh3; key: Note.Csh3 }
        Note { id: d3; key: Note.D3 }
        Note { id: dsh3; key: Note.Dsh3 }
        Note { id: e3; key: Note.E3 }
        Note { id: f3; key: Note.F3 }
        Note { id: fsh3; key: Note.Fsh3 }
        Note { id: g3; key: Note.G3 }
        Note { id: gsh3; key: Note.Gsh3 }
        Note { id: a3; key: Note.A3 }
        Note { id: ash3; key: Note.Ash3 }
        Note { id: b3; key: Note.B3 }
        Note { id: c4; key: Note.C4 }
        Note { id: csh4; key: Note.Csh4 }
        Note { id: d4; key: Note.D4 }
        Note { id: dsh4; key: Note.Dsh4 }
        Note { id: e4; key: Note.E4 }
        Note { id: f4; key: Note.F4 }
        Note { id: fsh4; key: Note.Fsh4 }
        Note { id: g4; key: Note.G4 }
        Note { id: gsh4; key: Note.Gsh4 }
        Note { id: a4; key: Note.A4 }
        Note { id: ash4; key: Note.Ash4 }
        Note { id: b4; key: Note.B4 }
    }

    component VisualNote : Control {
        id: vnote
        property bool played: false
        property int pos: 0
        property bool sharp: false
        property alias text: text.text
        readonly property real offset: pos * noteWidth

        onPlayedChanged: {
            anim.running = played
        }

        x: sharp ? offset + (noteWidth - sharpWidth / 2) : offset
        z: sharp ? 1 : 0

        background: Rectangle {
            implicitWidth: vnote.sharp ? sharpWidth : noteWidth
            implicitHeight: vnote.sharp ? sharpHeight : noteHeight
            border.color: vnote.sharp ? cNoteBorder : cNoteBorder
            color: vnote.played ? "red" : vnote.hovered ?
                (vnote.sharp ? cSharpHov : cNoteHov) :
                (vnote.sharp ? cSharp : cNote)

            ColorAnimation {
                id: anim
                target: vnote.background
                property: "color"
                to: "red"
                duration: 50
            }
        }

        Text {
            id: text
            anchors.horizontalCenter: parent.horizontalCenter
            horizontalAlignment: Text.AlignHCenter
            y: vnote.background.height * 0.75
            color: vnote.sharp ? cTextSharp : cText
            font.pixelSize: 12
        }
    }
    component Scale: Item {
        width: childrenRect.width
        required property int scaleNum
        property alias playedNoteC: c.played
        property alias playedNoteCsh: csh.played
        property alias playedNoteD: d.played
        property alias playedNoteDsh: dsh.played
        property alias playedNoteE: e.played
        property alias playedNoteF: f.played
        property alias playedNoteFsh: fsh.played
        property alias playedNoteG: g.played
        property alias playedNoteGsh: gsh.played
        property alias playedNoteA: a.played
        property alias playedNoteAsh: ash.played
        property alias playedNoteB: b.played
        VisualNote {
            id: c
            text: "C" + scaleNum
            pos: 0
        }
        VisualNote {
            id: csh
            text: "C#" + scaleNum
            sharp: true
            pos: 0
        }
        VisualNote {
            id: d
            text: "D" + scaleNum
            pos: 1
        }
        VisualNote {
            id: dsh
            text: "D#" + scaleNum
            sharp: true
            pos: 1
        }
        VisualNote {
            id: e
            text: "E" + scaleNum
            pos: 2
        }
        VisualNote {
            id: f
            text: "F" + scaleNum
            pos: 3
        }
        VisualNote {
            id: fsh
            text: "F#" + scaleNum
            sharp: true
            pos: 3
        }
        VisualNote {
            id: g
            text: "G" + scaleNum
            pos: 4
        }
        VisualNote {
            id: gsh
            text: "G#" + scaleNum
            sharp: true
            pos: 4
        }
        VisualNote {
            id: a
            text: "A" + scaleNum
            pos: 5
        }
        VisualNote {
            id: ash
            text: "A#" + scaleNum
            sharp: true
            pos: 5
        }
        VisualNote {
            id: b
            text: "B" + scaleNum
            pos: 6
        }
    }

    RowLayout {
        anchors.fill: parent
        anchors.centerIn: parent
        spacing: 0

        Scale {
            id: octave3Scale
            scaleNum: 3
            playedNoteC: c3.played
            playedNoteCsh: csh3.played
            playedNoteD: d3.played
            playedNoteDsh: dsh3.played
            playedNoteE: e3.played
            playedNoteF: f3.played
            playedNoteFsh: fsh3.played
            playedNoteG: g3.played
            playedNoteGsh: gsh3.played
            playedNoteA: a3.played
            playedNoteAsh: ash3.played
            playedNoteB: b3.played
        }
        Scale {
            id: octave4Scale
            scaleNum: 4
            playedNoteC: c4.played
            playedNoteCsh: csh4.played
            playedNoteD: d4.played
            playedNoteDsh: dsh4.played
            playedNoteE: e4.played
            playedNoteF: f4.played
            playedNoteFsh: fsh4.played
            playedNoteG: g4.played
            playedNoteGsh: gsh4.played
            playedNoteA: a4.played
            playedNoteAsh: ash4.played
            playedNoteB: b4.played
        }
    }

}
