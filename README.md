# qtcleveraudioplugin

> This is the proof-of-concept implementation, as highlighted in my
> [thesis](https://github.com/deeedob/thesis). It is feature incomplete, and
> I'm currently working on a rewrite.

Integration with the [CLAP](https://github.com/free-audio/clap) audio plugin
standard by utilizing [clap-rci](https://github.com/deeedob/clap-rci) in
combination with QtGrpc to bridge the gap between professional audio plugins
and Qt user interfaces by relying on remote GUIs. This repository holds the
client-side implementation for the api provided by *clap-rci*, tailored to Qt.

<p align="center">
    <img src="docs/demo.gif" width=77%>
</p>

## Prerequisites

Make sure you are using Qt Version >= `6.6.0`, which includes all required
modules, and ensure that gRPC is installed

## Getting Started

```bash
# init the repo with its submodules recursively
git submodule update --init --recursive

# create the build files for 'debug' or 'release' builds
cmake --preset release
# you may need to specify the paths to the appropriate Qt and/or gRPC versions
CMAKE_PREFIX_PATH="/path/to/qt;/path/to/grpc" cmake --fresh --preset release

# build the project
cmake --build build/release/
```

This will build the libraries `ClapInterface` and `ClapControls`, as well as
the example project. The example project consumes these libraries to provide
the CLAP plugin bundle: QtClapPlugins, which contains two simple audio plugins:
*Gain* and *Reveal*. A CLAP-compatible host can now load these plugins for
usage.
