// Copyright (C) 2024 The Qt Company Ltd.
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-Qt-Commercial

#include "gain/gainprocessor.h"
#include "reveal/revealprocessor.h"

#include <server/serverctrl.h>

#include <functional>
#include <vector>
#include <cstring>
#include <stdexcept>

namespace {

struct PluginEntry {
    using FnCreate = std::function<const clap_plugin *(const clap_host *)>;

    PluginEntry(const clap_plugin_descriptor *d, FnCreate &&func)
            : descriptor(d), create(std::move(func)) {}

    const clap_plugin_descriptor *descriptor;
    FnCreate create;
};

}

// NOLINTBEGIN

static std::vector<PluginEntry> gPlugins;
static std::string gPluginPath;

template <typename T>
static void addPlugin()
{
    gPlugins.emplace_back(T::descriptor(), [](const clap_host *host) -> const clap_plugin * {
        auto plugin = new T(gPluginPath, host);
        return plugin->clapPlugin();
    });
}

static bool clap_init(const char *plugin_path)
{
    gPluginPath = plugin_path;
    addPlugin<Gain>();
    addPlugin<Reveal>();
    return true;
}

static void clap_deinit()
{
    gPlugins.clear();
    gPluginPath.clear();
    ServerCtrl::instance().stop();
}

static uint32_t clap_get_plugin_count(const clap_plugin_factory * /*unused*/)
{
    return static_cast<uint32_t>(gPlugins.size());
}

static const clap_plugin_descriptor *clap_get_plugin_descriptor(const clap_plugin_factory * /*unused*/, uint32_t index)
{
    if (index >= gPlugins.size())
        throw std::invalid_argument("Invalid plugin index");
    return gPlugins[index].descriptor;
}

static const clap_plugin *clap_create_plugin(const clap_plugin_factory *, const clap_host *host, const char *plugin_id)
{
    for (const auto &plugin : gPlugins) {
        if (!strcmp(plugin.descriptor->id, plugin_id))
            return plugin.create(host);
    }
    return nullptr;
}

static const clap_plugin_factory g_clap_plugin_factory = {
    .get_plugin_count = clap_get_plugin_count,
    .get_plugin_descriptor = clap_get_plugin_descriptor,
    .create_plugin = clap_create_plugin,
};

const void *clap_get_factory(const char *factory_id)
{
    if (!strcmp(factory_id, CLAP_PLUGIN_FACTORY_ID))
        return &g_clap_plugin_factory;
    return nullptr;
}

// NOLINTEND

CLAP_EXPORT const clap_plugin_entry clap_entry = {
    CLAP_VERSION,
    clap_init,
    clap_deinit,
    clap_get_factory,
};
