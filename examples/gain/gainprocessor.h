// Copyright (C) 2024 The Qt Company Ltd.
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-Qt-Commercial

#ifndef GAINPROCESSOR_H
#define GAINPROCESSOR_H

#include <plugin/coreplugin.h>

using namespace RCLAP_NAMESPACE;

class Gain final : public CorePlugin
{
public:
    Gain(const std::string &pluginPath, const clap_host *host);
    static const clap_plugin_descriptor *descriptor();

private:
    bool init() noexcept override;
    void defineAudioPorts() noexcept;

private:
    uint32_t mChannelCount = 2;
};

#endif // GAINPROCESSOR_H
