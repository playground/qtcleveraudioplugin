// Copyright (C) 2024 The Qt Company Ltd.
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-Qt-Commercial

import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import Clap.Interface
import Clap.Controls

ClapWindow {
    id: root

    height: 480
    title: "QGain"
    width: 640


    menuBar: ClapMenuBar {
        infoText: root.title
        dragWindow: root
        ClapMenu {
            title: qsTr("File")
            Action {
                text: qsTr("Exit")
                onTriggered: Qt.exit(0)
            }
        }
    }

    ColumnLayout {
        anchors.fill: parent
        anchors.margins: 10
        spacing: 10

        Text {
            Layout.alignment: Qt.AlignHCenter
            Layout.topMargin: 30
            color: QClapStyle.color("bg3")
            font.bold: true
            font.pointSize: 37
            text: root.title
        }

        ClapDial {
            id: dial

            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            Layout.preferredHeight: parent.width / 3
            Layout.preferredWidth: parent.width / 3
            paramId: 0
        }

        Item {
            Layout.fillWidth: true
            Layout.preferredHeight: parent.width / 7

            Image {
                id: qtlogo
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 20
                anchors.rightMargin: 20
                height: parent.height
                width: height
                mipmap: true
                fillMode: Image.PreserveAspectFit
                source: "qrc:/qt/qml/Clap/Controls/images/qt.svg"
            }
        }
    }
}
