// Copyright (C) 2024 The Qt Company Ltd.
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-Qt-Commercial

#include "gainprocessor.h"
#include <plugin/modules/module.h>
#include <plugin/parameter/parameter.h>

// The GainModule controls all the processing of the plugin.
class GainModule final : public Module
{
public:
    explicit GainModule(Gain &plugin) : Module(plugin, "GainModule", 0)
    {
    }

    void init() noexcept override
    {
        mParamGain = addParameter(
            0, "gain",
            CLAP_PARAM_IS_AUTOMATABLE | CLAP_PARAM_IS_MODULATABLE | CLAP_PARAM_REQUIRES_PROCESS,
            std::make_unique<DecibelValueType>(-40.0, 40.0, 0)
        );
    }


    clap_process_status process(const clap_process *process, uint32_t frame) noexcept override
    {
        const float gain = static_cast<float>(mParamGain->engineValue());
        const float inputL = process->audio_inputs->data32[0][frame];
        const float inputR = process->audio_inputs->data32[1][frame];
        process->audio_outputs->data32[0][frame] = inputL * gain;
        process->audio_outputs->data32[1][frame] = inputR * gain;

        return CLAP_PROCESS_CONTINUE;
    }

private:
    Parameter *mParamGain = nullptr;
};


Gain::Gain(const std::string &pluginPath, const clap_host *host)
    : CorePlugin(Settings(pluginPath).withPluginDirExecutablePath("Gain"),
        descriptor(), host, std::make_unique<GainModule>(*this))
{}

const clap_plugin_descriptor *Gain::descriptor()
{
    static const char *features[] = {
        CLAP_PLUGIN_FEATURE_AUDIO_EFFECT,
        CLAP_PLUGIN_FEATURE_UTILITY,
        nullptr
    };
    static const clap_plugin_descriptor desc = {
        CLAP_VERSION,
        "com.qt.clapinterface.gain",
        "QGain",
        "Qt",
        "https://www.qt.io/",
        nullptr,
        nullptr,
        "0.1",
        "Qt Gain Plugin",
        features
    };
    return &desc;
}

bool Gain::init() noexcept
{
    if (!CorePlugin::init())
        return false;
    defineAudioPorts();
    return true;
}

void Gain::defineAudioPorts() noexcept
{
    assert(!isActive());
    clap_audio_port_info info {
        0,
        "main",
        CLAP_AUDIO_PORT_IS_MAIN,
        mChannelCount,
        nullptr,
        0
    };
    audioPortsInfoIn().clear();
    audioPortsInfoOut().clear();
    audioPortsInfoIn().push_back(info);
    audioPortsInfoOut().push_back(info);
}
