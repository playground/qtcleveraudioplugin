// Copyright (C) 2024 The Qt Company Ltd.
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-Qt-Commercial

#ifndef REVEALPROCESSOR_H
#define REVEALPROCESSOR_H

#include <plugin/coreplugin.h>

using namespace RCLAP_NAMESPACE;

class Reveal final : public CorePlugin
{
public:
    Reveal(const std::string &pluginPath, const clap_host *host);
    static const clap_plugin_descriptor *descriptor();

private:
    bool init() noexcept override;
    void defineAudioPorts() noexcept;
    void defineNotePorts() noexcept;

private:
    uint32_t m_channelCount = 2;
};

#endif // REVEALPROCESSOR_H
