// Copyright (C) 2024 The Qt Company Ltd.
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-Qt-Commercial

#include "revealprocessor.h"
#include <plugin/modules/module.h>
#include <plugin/parameter/parameter.h>

// The GainModule controls all the processing of the plugin.
class RevealModule final : public Module
{
public:
    explicit RevealModule(Reveal &plugin) : Module(plugin, "RevealModule", 0)
    {
    }

    void init() noexcept override
    {
    }

    clap_process_status process(const clap_process *process, uint32_t frame) noexcept override
    {
        const float inputL = process->audio_inputs->data32[0][frame];
        const float inputR = process->audio_inputs->data32[1][frame];
        process->audio_outputs->data32[0][frame] = inputL;
        process->audio_outputs->data32[1][frame] = inputR;
        return CLAP_PROCESS_CONTINUE;
    }

private:
};


Reveal::Reveal(const std::string &pluginPath, const clap_host *host)
    : CorePlugin(Settings(pluginPath).withPluginDirExecutablePath("Reveal"),
        descriptor(), host, std::make_unique<RevealModule>(*this))
{}

const clap_plugin_descriptor *Reveal::descriptor()
{
    static const char *features[] = {
        CLAP_PLUGIN_FEATURE_AUDIO_EFFECT,
        CLAP_PLUGIN_FEATURE_UTILITY,
        nullptr
    };
    static const clap_plugin_descriptor desc = {
        CLAP_VERSION,
        "com.qt.clapinterface.reveal",
        "QReveal",
        "Qt",
        "https://www.qt.io/",
        nullptr,
        nullptr,
        "0.1",
        "Qt Reveal Plugin",
        features
    };
    return &desc;
}

bool Reveal::init() noexcept
{
    if (!CorePlugin::init())
        return false;
    defineAudioPorts();
    defineNotePorts();
    return true;
}

void Reveal::defineAudioPorts() noexcept
{
    assert(!isActive());
    clap_audio_port_info info {
        0,
        "main",
        CLAP_AUDIO_PORT_IS_MAIN,
        m_channelCount,
        nullptr,
        0
    };
    audioPortsInfoIn().clear();
    audioPortsInfoOut().clear();
    audioPortsInfoIn().push_back(info);
    audioPortsInfoOut().push_back(info);
}

void Reveal::defineNotePorts() noexcept
{
    assert(!isActive());
    clap_note_port_info info {
        0,
        CLAP_NOTE_DIALECT_CLAP | CLAP_NOTE_DIALECT_MIDI,
        CLAP_NOTE_DIALECT_CLAP,
        "main",
    };
    notePortsInfoIn().clear();
    notePortsInfoOut().clear();
    notePortsInfoIn().push_back(info);
}
