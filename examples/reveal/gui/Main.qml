// Copyright (C) 2024 The Qt Company Ltd.
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-Qt-Commercial

import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import QtQuick.Effects

import Clap.Interface
import Clap.Controls

ClapWindow {
    id: root

    width: 720
    height: 580
    title: "QReveal"

    property clapEventNote currentNote: null

    Connections {
        target: ClapInterface
        function onNoteReceived(note) {
            if (note.type === ClapEventNote.NoteOff)
                return
            root.currentNote = note
        }
    }

    menuBar: ClapMenuBar {
        infoText: root.title
        dragWindow: root
        ClapMenu {
            title: qsTr("File")
            Action {
                text: qsTr("Exit")
                onTriggered: Qt.exit(0)
            }
        }
    }

    ColumnLayout {
        anchors.fill: parent
        anchors.margins: 10
        spacing: 5

        Text {
            Layout.alignment: Qt.AlignHCenter
            Layout.topMargin: 30
            color: QClapStyle.color("bg3")
            font.bold: true
            font.pointSize: 37
            text: root.title
        }

        Rectangle {
            Layout.preferredWidth: parent.width - 40
            Layout.preferredHeight: parent.width / 7
            Layout.alignment: Qt.AlignHCenter
            color: QClapStyle.color("bg0_h")
            radius: 10
            GridView {
                id: grid
                anchors.fill: parent
                anchors.margins: 10
                model: [
                    {
                        name: "noteId: ",
                        data: root.currentNote ? root.currentNote.noteId : "null"
                    },
                    {
                        name: "portIndex: ",
                        data: root.currentNote ? root.currentNote.portIndex : "null"
                    },
                    {
                        name: "channel: ",
                        data: root.currentNote ? root.currentNote.channel : "null"
                    },
                    {
                        name: "key: ",
                        data: root.currentNote ? root.currentNote.key : "null"
                    },
                    {
                        name: "value: ",
                        data: root.currentNote ? root.currentNote.value.toFixed(2) : "null"
                    },
                    {
                        name: "type: ",
                        data: root.currentNote ? root.currentNote.type : "null"
                    }
                ]
                cellWidth: width / 3
                cellHeight: height / 2
                delegate: Row {
                    id: row
                    property int myspacing: 10
                    Rectangle {
                        x: myspacing / 2
                        y: myspacing / 2
                        width: grid.cellWidth - myspacing
                        height: grid.cellHeight - myspacing
                        color: "transparent"
                        border.color: QClapStyle.color("bg")
                        border.width: 2
                        radius: 5
                        Text {
                            id: name
                            x: parent.width / 6
                            text: modelData.name
                            color: QClapStyle.color("fg")
                            anchors.verticalCenter: parent.verticalCenter
                        }
                        Text {
                            anchors.left: name.right
                            anchors.leftMargin: row.myspacing
                            text: modelData.data
                            color: QClapStyle.color("fg")
                            anchors.verticalCenter: parent.verticalCenter
                        }
                    }
                }
            }
        }

        ClapKeys {
            x: 50
        }

        Item {
            Layout.fillWidth: true
            Layout.preferredHeight: parent.width / 7
        }
    }
}
