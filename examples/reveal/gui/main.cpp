// Copyright (C) 2024 The Qt Company Ltd.
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-Qt-Commercial

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QCommandLineParser>
#include <QQuickWindow>
#include <QtQml/QQmlExtensionPlugin>
#include <qclapinterface.h>
#include <QLibraryInfo>
#include <QDirIterator>

Q_IMPORT_QML_PLUGIN(ClapInterfacePlugin)

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    // Load the QML entry point.
    QQmlApplicationEngine engine;
    engine.loadFromModule("RevealModule", "Main");
    if (engine.rootObjects().isEmpty()) {
        qFatal() << "Unable to load QML entry point";
        return -1;
    }

    auto *window = qobject_cast<QQuickWindow *>(engine.rootObjects()[0]);
    if (!window) {
        qFatal() << "Root object is not a window";
        return -1;
    }

    QCommandLineParser parser;
    parser.process(app);
    const auto args = parser.positionalArguments();
    if (args.length() == 2) {
        auto *interface = engine.singletonInstance<QClapInterface*>("Clap.Interface","ClapInterface");
        if (interface == nullptr) {
            qFatal() << "Unable to find ClapInterface instance";
            return -1;
        }
        interface->connect(args[0], args[1]);
    }
    return QGuiApplication::exec(); // Start the event loop.
}
